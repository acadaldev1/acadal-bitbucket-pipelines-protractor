# acadal-bitbucket-pipelines-protractor

## About

Based on [pdouble16/bitbucket-pipeline-browsers], but uses atlassian/default-image:2.04

Installs **protractor**, **jasmine** and **jasmine-spec-reporter** and ***webdriver-manager**, and runs **webdriver-manager update**.

Then installs your Angular app in into the /app folder.

##build and push new version
docker build -t acadal/bitbucket-pipelines-protractor:1.1 .
docker push acadal/bitbucket-pipelines-protractor:1.1